fastapi==0.75.0
motor==2.5.1
pydantic[email]
python-decouple==3.6
uvicorn==0.17.6
