import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../api/services/CustomerService';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  CustomersList: any = [];

  ngOnInit(): void {
    this.loadCustomers();
  }

  displayedColumns: string[] = ['fullname'];
  dataSource = this.CustomersList;

  constructor(
    public customerService: CustomerService
  ) { }

  loadCustomers(){
    return this.customerService.routeListCustomersCustomerGet().subscribe((data: {}) => {
      this.CustomersList = data['data'][0];
    })
  }

  deleteCustomer(data) {
    var index = (index = this.CustomersList.map((x) => {
      return x.issue_name;
    }).indexOf(data.issue_name));
    return this.customerService.routeDeleteCustomerCustomerIdDelete(data.id).subscribe((res) => {
      this.CustomersList.splice(index, 1);
      console.log('Customer deleted!');
    });
  }
}