# Desafio CloudOpss
O presente projeto tem como por objetivo cumprir com o [desafio proposto](CHALLENGE.pdf) pela CloudOpss.

Pensei em adequar à LGPD mas a curva de aprendizado invibializaria a execução dadas as circunstancias do presente além de não constar como um requisito.

Em resumo, cadastro de clientes com:
    - Nome
    - Email
    - Telefone
    - Endereço completo
        - Logradouro
        - Número
        - Complemento
        - Bairro
        - Cidade
        - Estado
        - CEP
    - Profissão

(GET, POST, DELETE e UPDATE)

## Requisitos
1. Python REST API
2. Front End para gerenciar as informações e enviar currículo.
3. Ambos em container (DB, Back e Frontend)
4. Circuito de desenvolvimento (?) ([gitflow](https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow))
5. Repositório git com README e instruções de instalação.

## Extras:
1. Gerar imagem via GitLab CI
2. Teste de capacidade (?)
3. Teste unitário (?)

5 dias para entregar, iniciei 10/03/2022 14h50


## 1 - Python REST API
    - GET, POST, DELETE E UPDATE
    - Logs nível 1,2,3,4,5
    - Suporte banco de dados nosql
    - Tenha documentação declarada através de um endpoint (podendo ser Swagger)

## 2 - FrontEnd Interface
    - Botão "novo cadastro"
    - Nome
    - Email
    - Telefone
    - Endereço completo
    - Profissão
    - Upload de Currículo
    - Botão para Limpar
    - Botão para Enviar
    - PopUp de envio com sucesso
